# React Quiz App

My first fully functional **React** website with front-end and back-end.

Back-end is **required** to be running, in order to work the App.

## Front-end part

There is two main part of it:
1. **Welcome page / play Quiz**
   - Start screen : Need to enter your name to start, **if** number of questions != 0
   - Quiz screen : Dynamically reloads with new question every time
   - Finish screen : It shows your results with your name
2. **Question manager**
   - Question list : Checks all the available questions with the answers.
   - Question form : Adds new question to the list

The client side part was made with React. The following npm packages were used:
- `react-router-dom`
- `axios`
- `bootstrap`

## Back-end part

The server side part was made with Node.js.

Please ignore the quality of the backend, I'll try to recreate using promise wrapper with async/await in the near future.

The following npm packages were used:
- `express`
- `mysql2`
- `cors`
- `nodemon`
