import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { QuestionList } from './QuestionList';

const initialValues = {
    question: "",
    answer_1: "",
    answer_2: "",
    answer_3: "",
    answer_4: "",
    correctAnswer: ""
};

export function QuestionFormAdd() {
    // form inputok tárolása
    const [values, setValues] = useState(initialValues);
    const handleInputChange = (e) => {
        const { name, value } = e.target;

        setValues({
            ...values,
            [name]: value,
        });
    };

    const handleRadioInputChange = (e) => {
        const { value } = e.target;
        setValues({
            ...values,
            correctAnswer: value,
        });
    }

    function removeQuestion(id) {
        let filltered = questionList.filter((q) => {
            return q.id !== id
        })
        setQuestionList(filltered)
    }

    // kérdések lekérése / tárolása
    const [questionList, setQuestionList] = useState([]);
    useEffect(() => {
        Axios.get('http://localhost:3001/api/getQuestion')
            .then((response) => {
                setQuestionList(response.data);
            })
    }, [])

    // form inputok backendre küldése
    const submitQuiz = () => {
        Axios.post("http://localhost:3001/api/insert", {
            question: values.question,
            answer_1: values.answer_1,
            answer_2: values.answer_2,
            answer_3: values.answer_3,
            answer_4: values.answer_4,
            correctAnswer: values.correctAnswer
        });
    }

    // minden mező kitöltve checker
    const formFilled = () => {
        const formIsFilled =
            values.question !== '' &&
            values.answer_1 !== '' &&
            values.answer_2 !== '' &&
            values.answer_3 !== '' &&
            values.answer_4 !== '' &&
            values.correctAnswer;
        return formIsFilled;
    }

    return (
        <div className="jumbotron">
            <QuestionList questions={questionList} removeQuestion={removeQuestion} />
            <div className="row justify-content-center align-items-center">
                <form className="col-sm-5 mt-5">
                    <h2 className="text-center">Add a question!</h2>
                    <h5 className="py-2">Question:</h5>
                    <div className="form-group form-inline">
                        <input
                            className="form-control flex-fill mr-4"
                            autoComplete="off"
                            value={values.question}
                            name="question"
                            onChange={handleInputChange}
                        />
                    </div>
                    <h5 className="py-2">Answers:</h5>
                    <div className="form-group form-inline">
                        <textarea
                            className="form-control flex-fill mr-2"
                            rows="1"
                            value={values.answer_1}
                            name="answer_1"
                            onChange={handleInputChange}>
                        </textarea>
                        <input type="radio" name="correct" value="1" onClick={handleRadioInputChange}></input>
                    </div>
                    <div className="form-group form-inline">
                        <textarea
                            className="form-control flex-fill mr-2"
                            rows="1"
                            value={values.answer_2}
                            name="answer_2"
                            onChange={handleInputChange}>
                        </textarea>
                        <input type="radio" name="correct" value="2" onClick={handleRadioInputChange}></input>
                    </div>
                    <div className="form-group form-inline">
                        <textarea
                            className="form-control flex-fill mr-2"
                            rows="1"
                            value={values.answer_3}
                            name="answer_3"
                            onChange={handleInputChange}>
                        </textarea>
                        <input type="radio" name="correct" value="3" onClick={handleRadioInputChange}></input>
                    </div>
                    <div className="form-group form-inline">
                        <textarea
                            className="form-control flex-fill mr-2"
                            rows="1"
                            value={values.answer_4}
                            name="answer_4"
                            onChange={handleInputChange}>
                        </textarea>
                        <input type="radio" name="correct" value="4" onClick={handleRadioInputChange}></input>
                    </div>
                    <div className="col text-center">
                        {
                            formFilled() ?
                                <button type="submit" className="btn btn-success" onClick={submitQuiz}>Send</button> :
                                <button className="btn btn-danger" disabled={true}>Send</button>
                        }
                    </div>
                </form>
            </div>
        </div>
    );
}