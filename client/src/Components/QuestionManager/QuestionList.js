import Axios from 'axios';

export function QuestionList({ questions, removeQuestion }) {

    const deleteQuiz = (id) => {
        removeQuestion(id);
        Axios.delete(`http://localhost:3001/api/delete/${id}`)
    }

    return (
        <div className="row justify-content-center align-items-center">
            <div>
                {questions.map((adat, index) => (
                    <div key={adat.id} className="card mb-4">
                        <div className="card-header ">
                            <div style={{ width: 350, fontWeight: "bold" }}>
                                {index + 1}. {adat.question}
                                <button
                                    type="button"
                                    className="btn btn-outline-danger float-right"
                                    onClick={() => {
                                        deleteQuiz(adat.id)
                                    }}
                                >
                                    <img src="trash.svg" alt="Törlés" />
                                </button>
                            </div>
                        </div>
                        <div >
                            <ul className="list-group list-group-flush">
                                <li
                                    className="list-group-item"
                                    style={{ backgroundColor: adat.correctAnswer === "1" ? 'lightgreen' : '#FDCBCC' }}
                                >
                                    {adat.answer_1}
                                </li>
                                <li
                                    className="list-group-item"
                                    style={{ backgroundColor: adat.correctAnswer === "2" ? 'lightgreen' : '#FDCBCC' }}
                                >
                                    {adat.answer_2}
                                </li>
                                <li
                                    className="list-group-item"
                                    style={{ backgroundColor: adat.correctAnswer === "3" ? 'lightgreen' : '#FDCBCC' }}
                                >
                                    {adat.answer_3}
                                </li>
                                <li
                                    className="list-group-item"
                                    style={{ backgroundColor: adat.correctAnswer === "4" ? 'lightgreen' : '#FDCBCC' }}
                                >
                                    {adat.answer_4}
                                </li>
                            </ul>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}