import { NavLink } from "react-router-dom";

export function NavBar() {
    return (
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark justify-content-center rounded">
            <ul className="navbar-nav lead">
                <li className="nav-item">
                    <NavLink to={`/`} activeClassName='active' exact>
                        <span className="nav-link px-5">Play Quiz</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to={`/question-manager`} activeClassName='active'>
                        <span className="nav-link px-5">Question Manager</span>
                    </NavLink>
                </li>
            </ul>
        </nav>
    );
}