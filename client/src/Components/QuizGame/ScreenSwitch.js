import React from 'react';
import Axios from 'axios';
import { useState, useEffect, createContext } from 'react';
import { StartScreen } from './StartScreen/StartScreen';
import { QuizScreen } from './QuizScreen/QuizScreen';
import { FinishScreen } from './FinishScreen/FinishScreen';

export const QuizContext = createContext();

export function ScreenSwitch() {
    const [gameState, setGameState] = useState("menu");
    const [score, setScore] = useState(0);
    const [name, setName] = useState("");

    // kérdések lekérése / tárolása
    const [questionList, setQuestionList] = useState([]);
    useEffect(() => {
        Axios.get('http://localhost:3001/api/getQuestion')
            .then((response) => {
                setQuestionList(response.data);
            })
    }, [])

    return (
        <div>
            <QuizContext.Provider value={{ gameState, setGameState, score, setScore, questionList, name, setName }}>
                {gameState === "menu" && <StartScreen />}
                {gameState === "quiz" && <QuizScreen />}
                {gameState === "finish" && <FinishScreen />}
            </QuizContext.Provider>
        </div>
    )
}