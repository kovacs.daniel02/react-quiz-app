import React, { useContext } from 'react';
import { QuizContext } from '../ScreenSwitch';

export function FinishScreen() {
    const { score, setScore, setGameState, questionList, name } = useContext(QuizContext);

    const restartQuiz = () => {
        setScore(0)
        setGameState("menu")
    }

    return (
        <div className="jumbotron d-flex align-items-center justify-content-center" style={{ height: 600 }}>
            <div className="col text-center">
                <h1>Quiz finished!</h1>
                <h3 className="my-5">{name}, you scored {score} / {questionList.length} correct!</h3>
                <div className="col text-center mt-5">
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={restartQuiz}
                    >Play again
                </button>
                </div>
            </div>
        </div>
    )
}