import React, { useContext, useState } from 'react';
import { QuizContext } from '../ScreenSwitch';


export function StartScreen() {
    const { setGameState, setName, questionList } = useContext(QuizContext);
    const [value, setValue] = useState("");

    const handleInputChange = (e) => {
        const { value } = e.target;
        setValue(value);
        setName(value);
    };

    const notEmpty = () => {
        return value !== "" &&
            questionList.length !== 0;
    }

    return (
        <div className="jumbotron d-flex align-items-center justify-content-center" style={{ height: 600 }}>
            <div>
                <h1>Welcome to the Quiz App</h1>
                <form className="col-md-8 mx-auto mt-5 text-center">
                    <div className="form-group">
                        <label>Please enter your name: </label>
                        <input
                            type="text"
                            className="form-control"
                            onChange={handleInputChange}
                        />
                        <div className="col text-center">
                            {
                                notEmpty() ?
                                    <button
                                        type="button"
                                        className="btn btn-success mt-5"
                                        onClick={() => { setGameState("quiz") }}
                                    >
                                        Start game
                                    </button> :
                                    <button className="btn btn-danger mt-5" disabled={true}>Start game</button>
                            }
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}