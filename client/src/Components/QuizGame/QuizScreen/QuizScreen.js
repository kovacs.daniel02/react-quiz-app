import React, { useState, useContext } from 'react';
import { QuizContext } from '../ScreenSwitch';

export function QuizScreen() {
    const { score, setScore, setGameState, questionList } = useContext(QuizContext);

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [optionChosen, setOptionChosen] = useState("");

    const nextQuestion = () => {
        if (questionList[currentQuestion].correctAnswer === optionChosen) {
            setScore(score + 1);
        }
        setCurrentQuestion(currentQuestion + 1);
    }

    const finishQuiz = () => {
        if (questionList[currentQuestion].correctAnswer === optionChosen) {
            setScore(score + 1);
        }
        setGameState("finish");
    }

    return (
        <div className="jumbotron" style={{ height: 600 }}>
            <div className="text-center mb-5">
                <h1>{questionList[currentQuestion].question}</h1>
            </div>
            <div className="d-flex align-items-center justify-content-center">
                <div className="w-50">
                    <button onClick={() => setOptionChosen("1")} className="btn btn-secondary btn-lg btn-block mb-3">{questionList[currentQuestion].answer_1}</button>
                    <button onClick={() => setOptionChosen("2")} className="btn btn-secondary btn-lg btn-block mb-3">{questionList[currentQuestion].answer_2}</button>
                    <button onClick={() => setOptionChosen("3")} className="btn btn-secondary btn-lg btn-block mb-3">{questionList[currentQuestion].answer_3}</button>
                    <button onClick={() => setOptionChosen("4")} className="btn btn-secondary btn-lg btn-block mb-3">{questionList[currentQuestion].answer_4}</button>
                </div>
            </div>
            <div className="col text-center mt-5">
                {currentQuestion === questionList.length - 1 ? (
                    <button onClick={finishQuiz} className="btn btn-primary">Finish quiz</button>
                ) : (
                    <button onClick={nextQuestion} className="btn btn-primary">Next question</button>
                )}
            </div>
        </div>
    )
}
