import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { NavBar } from './Components/NavBar/NavBar';
import { QuestionFormAdd } from './Components/QuestionManager/QuestionFormAdd';
import { ScreenSwitch } from './Components/QuizGame/ScreenSwitch'


function App() {
    return (
        <Router>
            <div className="container">
                <NavBar />
                <Switch>
                    <Route exact path="/" component={ScreenSwitch} />
                    <Route exact path="/question-manager" component={QuestionFormAdd} />
                    <Redirect to={"/"} />
                </Switch>
            </div>
        </Router>
    );
}

export default App;
