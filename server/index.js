const express = require('express');
const app = express();
const cors = require('cors');
const mysql = require('mysql2');
const port = 3001;

// MYSQL DEFAULT CONNECTION
const db = mysql.createConnection({
    host: '46.101.185.21',
    user: 'szasz_edu',
    password: 'SzaszSQL2021',
    database: 'Dancsok_Web_Beadando',
});

app.use(cors());
app.use(express.json());

// READ
app.get("/api/getQuestion", (req, res) => {
    const sqlSelect = "SELECT * FROM Questions";

    db.query(sqlSelect, (err, result) => {
        res.send(result);
    });
})

// CREATE
app.post("/api/insert", (req, res) => {
    const question = req.body.question
    const answer_1 = req.body.answer_1
    const answer_2 = req.body.answer_2
    const answer_3 = req.body.answer_3
    const answer_4 = req.body.answer_4
    const correctAnswer = req.body.correctAnswer

    const sqlQuestionInsert = "INSERT INTO Questions (question, answer_1, answer_2, answer_3, answer_4, correctAnswer) VALUES (?,?,?,?,?,?)";

    db.query(sqlQuestionInsert, [question, answer_1, answer_2, answer_3, answer_4, correctAnswer], (err, result) => {});
});

// DELETE
app.delete("/api/delete/:id", (req, res) => {
    const id = req.params.id;
    const sqlDeleteAnswers = "DELETE from Questions where id = ?";

    db.query(sqlDeleteAnswers, id, (err, result) => {})
})


app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});